package epitech.java.jcsi.dao;

import java.util.ArrayList;
import java.util.List;

import epitech.java.jcsi.core.CD;
import epitech.java.jcsi.core.ModelObject;
import epitech.java.jcsi.core.Product;
import epitech.java.jcsi.storage.ObjectMapper;
import epitech.java.jcsi.storage.StorageException;
import epitech.java.jcsi.storage.WhereClause;

public class ProductDAO {
	/**
	 * Mapper instance
	 */
	private ObjectMapper objectMapper;
	
	public ProductDAO(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}
	
	public void creatProduct(Product product) throws DAOException {
		try {
			product = (Product) objectMapper.createObject(product);
		} catch (StorageException e) {
			throw new DAOException("Could not creat Product : " + product);
		}
	}
	
	public Product getProduct(int productId) throws DAOException {
		try {
			return (Product) objectMapper.readObject(Product.class, productId);
		} catch (StorageException e) {
			throw new DAOException("Could not get Product( " + productId + ") "+ e);
		}
	}
	
	public void updateProduct(Product product) throws DAOException {
		try {
			product = (Product) objectMapper.updateObject(product);
		} catch (StorageException e) {
			throw new DAOException("Could not update Product : " + product);
		}
	}
	
	public void deleteProduct(Product product) throws DAOException {
		try {
			objectMapper.deleteObject(product);
		} catch (StorageException e) {
			throw new DAOException("Could not delete Product : " + product);
		}
	}

	public List<Product> getAllProducts() throws DAOException {
		List<ModelObject> list = null;
		WhereClause clause = new WhereClause();
		clause.where("1", "1");
		try {
			list = objectMapper.findObject(CD.class, clause);
		} catch (StorageException e) {
			throw new DAOException("Could not get Products ", e);
		}		
		List<Product> l2 = new ArrayList<Product>();
		for (ModelObject mo : list) {
			l2.add((Product) mo);
		}
		return l2;
	}	
}
