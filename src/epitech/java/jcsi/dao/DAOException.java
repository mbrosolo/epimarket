package epitech.java.jcsi.dao;

public class DAOException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3048027755061737163L;

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            The message
	 * @param cause
	 *            The cause or null
	 */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOException(String message) {
		super(message);
	}
}
