package epitech.java.jcsi.dao;

import java.util.List;

import epitech.java.jcsi.core.Account;
import epitech.java.jcsi.core.ModelObject;
import epitech.java.jcsi.storage.ObjectMapper;
import epitech.java.jcsi.storage.StorageException;
import epitech.java.jcsi.storage.WhereClause;

/**
 * DAO for Account object.
 * @author sornet_r
 */
public class AccountDAO {
	/**
	 * Mapper instance
	 */
	private ObjectMapper objectMapper;
	
	public AccountDAO(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	public void createAccount(Account account) throws DAOException {
		try {
			account = (Account) objectMapper.createObject(account);
		} catch (StorageException e) {
			throw new DAOException("Could not create account : ", e);
		}
	}
	
	public Account getAccount(int accountId) throws DAOException {
		try {
			return (Account) objectMapper.readObject(Account.class, accountId);
		} catch (StorageException e) {
			throw new DAOException("Could not get account ", e);
		}
	}
	
	public void updateAccount(Account account) throws DAOException {
		try {
			account = (Account) objectMapper.updateObject(account);
		} catch (StorageException e) {
			throw new DAOException("Could not update account ", e);
		}
	}
	
	public void deleteAccount(Account account) throws DAOException {
		try {
			objectMapper.deleteObject(account);
		} catch (StorageException e) {
			throw new DAOException("Could not delete account ", e);
		}
	}

	public Account findByName(String login, String password) throws DAOException {
		WhereClause clause = new WhereClause();
		Account account = null;
		clause.where("login", login);
		clause.where("digest", password);
		List<ModelObject> list = null;
		try {
			 list = objectMapper.findObject(Account.class, clause);
		} catch (StorageException e) {
			throw new DAOException("Could not get account: " + login , e);
		}
		if (list.isEmpty())
			throw new DAOException("Account"+login+" does not exist or incorrect password ");
		return (Account) list.get(0);
	}	
}
