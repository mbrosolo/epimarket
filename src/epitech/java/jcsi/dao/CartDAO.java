package epitech.java.jcsi.dao;

import epitech.java.jcsi.core.Cart;
import epitech.java.jcsi.storage.ObjectMapper;
import epitech.java.jcsi.storage.StorageException;

/**
 * DAO for Cart object. 
 * @author sornet_r
 */
public class CartDAO {
	/**
	 * Mapper instance
	 */
	private ObjectMapper objectMapper;

	public CartDAO(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}
	
	public void creatCart(Cart cart) throws DAOException {
		try {
			cart = (Cart) objectMapper.createObject(cart);
		} catch (StorageException e) {
			throw new DAOException("Could not creat Cart : " + cart);
		}
	}
	
	public Cart getCart(int cartId) throws DAOException {
		try {
			return (Cart) objectMapper.readObject(Cart.class, cartId);
		} catch (StorageException e) {
			throw new DAOException("Could not get Cart( " + cartId + ") "+ e);
		}
	}
	
	public void updateCart(Cart cart) throws DAOException {
		try {
			cart = (Cart) objectMapper.updateObject(cart);
		} catch (StorageException e) {
			throw new DAOException("Could not update Cart : " + cart);
		}
	}
	
	public void deleteCart(Cart cart) throws DAOException {
		try {
			objectMapper.deleteObject(cart);
		} catch (StorageException e) {
			throw new DAOException("Could not delete Cart : " + cart);
		}
	}	
}
