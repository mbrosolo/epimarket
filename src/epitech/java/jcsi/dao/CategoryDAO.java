package epitech.java.jcsi.dao;

import epitech.java.jcsi.core.Category;
import epitech.java.jcsi.storage.ObjectMapper;
import epitech.java.jcsi.storage.StorageException;

/**
 * DAO for Category object.
 * @author sornet_r
 */
public class CategoryDAO {
	/**
	 * Mapper instance
	 */
	private ObjectMapper objectMapper;

	public CategoryDAO(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}
	
	
	public void creatCategory(Category category) throws DAOException {
		try {
			category = (Category) objectMapper.createObject(category);
		} catch (StorageException e) {
			throw new DAOException("Could not creat Category : " + category);
		}
	}
	
	public Category getCategory(int categoryId) throws DAOException {
		try {
			return (Category) objectMapper.readObject(Category.class, categoryId);
		} catch (StorageException e) {
			throw new DAOException("Could not get Category( " + categoryId + ") "+ e);
		}
	}
	
	public void updateCategory(Category category) throws DAOException {
		try {
			category = (Category) objectMapper.updateObject(category);
		} catch (StorageException e) {
			throw new DAOException("Could not update Category : " + category);
		}
	}
	
	public void deleteCategory(Category category) throws DAOException {
		try {
			objectMapper.deleteObject(category);
		} catch (StorageException e) {
			throw new DAOException("Could not delete Category : " + category);
		}
	}	
}
