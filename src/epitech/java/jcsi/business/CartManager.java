package epitech.java.jcsi.business;

import epitech.java.jcsi.core.Cart;
import epitech.java.jcsi.core.Product;
import epitech.java.jcsi.core.OrderLine;
import epitech.java.jcsi.core.exception.CartException;
import epitech.java.jcsi.dao.CartDAO;
import epitech.java.jcsi.dao.DAOException;
import epitech.java.jcsi.storage.ObjectMapper;

/**
 * CartManager is a manager class for cart product.
 * @author sornet_r
 *
 */
public class CartManager {

	private CartDAO dao = null;

	public CartManager(ObjectMapper objectMapper) {
		this.dao = new CartDAO(objectMapper);
	}
	
	/**
	 * Add a quantity of products to a cart.
	 * 
	 * @param cart
	 *            The cart to add products
	 * @param product
	 *            The product to add
	 * @param quantity
	 *            The quantity of products to add
	 * @throws CartException
	 *             Upon storage exception
	 */
	public void addToCart(Cart cart, Product product, int quantity) throws CartException {
		if (quantity <= 0)
			throw new CartException("Couldn't add product : Wrong quantity (" + quantity + ")");
		cart.addOrderLine(new OrderLine(cart, product, quantity));
//		try {
//			dao.updateCart(cart);
//		} catch (DAOException e) {
//			throw new CartException("Couldn't add product : " + product + " to cart : " + cart);
//		}
	}
	
	/**
	 * Update cart with a new value.
	 *  If product doesn't exist add it to cart
	 *  If product count is 0 remove from cart
	 * @param cart
	 * 			Cart to update from
	 * @param product
	 * 			Product to update
	 * @param quantity
	 * 			New Quantity
	 * @throws CartException
	 * 			Upon database update failure or wrong argument
	 */
	public void updateCart(Cart cart, Product product, int quantity) throws CartException {
		if (quantity < 0)
			throw new CartException("Couldn't add product : Wrong quantity (" + quantity + ")");

		for (OrderLine ol : cart.getOrderLines())
			if (ol.getProduct().equals(product)) {
				if (quantity == 0)
					cart.getOrderLines().remove(ol);
				else
					ol.setQuantity(quantity);
				break;
			}
		try {
			dao.updateCart(cart);
		} catch (DAOException e) {
			throw new CartException("Couldn't update product : " + product + " to cart : " + cart);
		}
	}
	
	/**
	 * Get cart
	 * @param cartId
	 * 				The cart id in database
	 * @return
	 * 				Cart
	 * @throws CartException
	 * 				Upon storage exception
	 */
	public Cart getCart(int cartId) throws CartException {
			try {
				return this.dao.getCart(cartId);
			} catch (DAOException e) {
				throw new CartException("Could not get the cart " + cartId);
			}
	}
}

