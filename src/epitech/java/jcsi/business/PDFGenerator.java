package epitech.java.jcsi.business;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import epitech.java.jcsi.core.Account;
import epitech.java.jcsi.core.Cart;
import epitech.java.jcsi.core.OrderLine;
import epitech.java.jcsi.core.exception.PDFException;

public class PDFGenerator {	
	static public void generatePDF(HttpServletResponse response, Account account, Cart cart) throws PDFException {
		Document document = new Document();
		response.setContentType("application/pdf");
		try {
			PdfWriter.getInstance(document, response.getOutputStream());
			document.open();
			generateBill(document, account, cart);
		} catch (IOException e1) {
			throw new PDFException("Couldn't open stream");
		} catch (DocumentException e) {
			throw new PDFException("Couldn't generate PDF : " + e);
		} finally {
			document.close();
		}
	}
	
	static public void generateBill(Document document, Account account, Cart cart) throws DocumentException {
		document.addAuthor("Epimarket");
		document.addCreationDate();
		document.addTitle("Epimarket Bill");

		Font f = new Font();
		f.setStyle("bold");
		
		Paragraph p1 = new Paragraph();
		p1.add(new Chunk("Epimarket", f));
		p1.add(Chunk.NEWLINE);
		p1.add(new Chunk("42 Neverland Street"));
		p1.add(Chunk.NEWLINE);
		p1.add(new Chunk("Neverland"));
		p1.add(Chunk.NEWLINE);
		p1.add(new Chunk("epimarket@contact.nl"));
		p1.add(Chunk.NEWLINE);

		document.add(p1);
		document.add(Chunk.NEWLINE);
		
		Paragraph p2 = new Paragraph();
		p2.setAlignment(Paragraph.ALIGN_RIGHT);
		p2.add(new Chunk(account.getName() + " " + account.getSurname(), f));
		p2.add(Chunk.NEWLINE);
		p2.add(new Chunk(account.getMail()));
		document.add(p2);
		document.add(Chunk.NEWLINE);

		Paragraph p3 = new Paragraph();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		p3.add(new Chunk("Epimarket Bill : ", f));
		p3.add(new Chunk(dateFormat.format(date)));
		document.add(p3);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		
		PdfPTable table = new PdfPTable(4);
		
		table.addCell("Name");
		table.addCell("Quantity");
		table.addCell("Price");
		table.addCell("Total");
		
		double totalOrder = 0;
		
		for (OrderLine ol : cart.getOrderLines()) {
			Double total = ol.getProduct().getPrice() * ol.getQuantity();
			totalOrder += total;
			table.addCell(ol.getProduct().getName());
			table.addCell(ol.getQuantity().toString());
			table.addCell(ol.getProduct().getPrice().toString());
			table.addCell(total.toString());
		}

		table.addCell("");
		table.addCell("");
		table.addCell("Total :");
		table.addCell(Double.toString(totalOrder));
		
		document.add(table);
	}

}
