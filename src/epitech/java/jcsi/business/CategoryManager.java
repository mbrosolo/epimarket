package epitech.java.jcsi.business;

import epitech.java.jcsi.core.Category;
import epitech.java.jcsi.core.Product;
import epitech.java.jcsi.dao.CategoryDAO;
import epitech.java.jcsi.dao.DAOException;
import epitech.java.jcsi.core.exception.CategoryException;
import epitech.java.jcsi.storage.ObjectMapper;

/**
 * CategoryManager is a manager for Category. 
 * @author sornet_r
*/
public class CategoryManager {

	CategoryDAO dao = null;
	
	public CategoryManager(ObjectMapper objectMapper) {
		dao = new CategoryDAO(objectMapper);
	}
	
	/**
	 * Get Category
	 * @param cartId
	 * 				The category id in database
	 * @return
	 * 				Category
	 * @throws CategoryException
	 * 				Upon storage exception
	 */
	public Category getCategory(int categoryId) throws CategoryException {
		try {
			return dao.getCategory(categoryId);
		} catch (DAOException e) {
			throw new CategoryException("Couldn't get category (" + categoryId + ")");
		}
	}

	/**
	 * Add a Category in Database
	 * @param category
	 * 			Category to create in database
	 * @throws CategoryException
	 * 			Upon database failure
	 */
	public void creatCategory(Category category) throws CategoryException {
		try {
			this.dao.creatCategory(category);
		} catch (DAOException e) {
			throw new CategoryException("Couldn't delete " + category.getName());
		}
	}
	
	/**
	 * Delete a category in Database
	 * @param category
	 * 			Category to delete in database
	 * @throws CategoryException
	 * 			Upon database failure
	 */
	public void delCategory(Category category) throws CategoryException {
		try {
			this.dao.deleteCategory(category);
		} catch (DAOException e) {
			throw new CategoryException("Couldn't delete " + category);
		}
	}
	
	/**
	 * Add a child category in a main category
	 * @param category
	 * 				Main category
	 * @param child
	 * 				Child category to add in main one
	 * @throws CategoryException
	 * 				Upon database update failure
	 */
	public void addChildCategory(Category category, Category child) throws CategoryException {
		category.addChildCategory(child);
		try {
			this.dao.updateCategory(category);
		} catch (DAOException e) {
			throw new CategoryException("Couldn't update " + category);
		}
	}
	
	/**
	 * Delete a child category in a main category
	 * @param category
	 * 				Main category
	 * @param child
	 * 				Child category to delete in main one
	 * @throws CategoryException
	 * 				Upon database update failure
	 */
	public void delChildCategory(Category category, Category child) throws CategoryException {
		category.delChildCategory(child);
		try {
			this.dao.updateCategory(category);
		} catch (DAOException e) {
			throw new CategoryException("Couldn't update " + category);
		}
	}
	
	public void addProduct(Category category, Product product) throws CategoryException {
		category.addProduct(product);
		try {
			this.dao.updateCategory(category);
		} catch (DAOException e) {
			throw new CategoryException("Couldn't update " + category);
		}
	}
	
	public void delProduct(Category category, Product product) throws CategoryException {
		category.delProduct(product);
		try {
			this.dao.updateCategory(category);
		} catch (DAOException e) {
			throw new CategoryException("Couldn't update " + category);
		}
	}
}
