package epitech.java.jcsi.business;

import java.util.List;

import epitech.java.jcsi.core.Product;
import epitech.java.jcsi.core.exception.ProductException;
import epitech.java.jcsi.dao.DAOException;
import epitech.java.jcsi.dao.ProductDAO;
import epitech.java.jcsi.storage.ObjectMapper;

public class ProductManager {

	ProductDAO dao = null;
	
	public ProductManager(ObjectMapper objectMapper) {
		dao = new ProductDAO(objectMapper);
	}
	
	public Product getProduct(int productId) throws ProductException {
		try {
			return dao.getProduct(productId);
		} catch (DAOException e) {
			throw new ProductException("Couldn't get product " + productId);
		}
	}
	
	public void delProduct(Product product) throws ProductException {
		try {
			dao.deleteProduct(product);
		} catch (DAOException e) {
			throw new ProductException("Couldn't delete product");
		}
	}
	
	public List<Product> getAllProducts() throws ProductException {
		List<Product> list = null;
		try {
			dao.getAllProducts();
		} catch (DAOException e) {
			throw new ProductException("Couldn't get products", e);
		}	
		return list;
	}
}