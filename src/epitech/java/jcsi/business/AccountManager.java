package epitech.java.jcsi.business;

import epitech.java.jcsi.core.Account;
import epitech.java.jcsi.dao.AccountDAO;
import epitech.java.jcsi.dao.DAOException;
import epitech.java.jcsi.core.exception.AccountException;
import epitech.java.jcsi.storage.ObjectMapper;

/**
 * Manager for Account object. 
 * @author sornet_r
 */
public class AccountManager {
	
	/**
	 * DAO account
	 */
	private AccountDAO dao = null;

	/**
	 * Constructor
	 *
	 * @param objectMapper
	 * 				Storage object
	 */
	public AccountManager(ObjectMapper objectMapper) {
		this.dao = new AccountDAO(objectMapper);
	}

	/**
	 * Get account
	 * @param accountId
	 * 				Account id in database
	 * @return
	 * 				Account
	 * @throws CartException
	 * 				Upon storage exception
	 */
	public Account getAccount(int accountId) throws AccountException {
		try {
			return this.dao.getAccount(accountId);
		} catch (DAOException e) {
			throw new AccountException("Couldn't get account");
		}
	}
	
	public Account registerAccount(String login, String digest, String name, String surname, String mail) throws AccountException {
		Account account = new Account(login, digest, name, surname, mail);
		try {
			this.dao.createAccount(account);
		} catch  (DAOException e) {
			throw new AccountException("Couldn't register account", e);
		}
		return account;
	}
	
	public void deleteAccount(Account account) throws AccountException {
		try {
			this.dao.deleteAccount(account);
		} catch (DAOException e) {
			throw new AccountException("Couldn't delete account");
		}
	}
	
	/**
	 * @param login
	 * 			Account Login
	 * @param password
	 * 			Account Password
	 * @return
	 * 			Logged Account
	 * @throws AccountException
	 * 			Upon login failure
	 */
	public Account loginAccount(String login, String password) throws AccountException {
		Account account = null;
		try {
			account = this.dao.findByName(login, password);
		} catch (DAOException e) {
			throw new AccountException("Could not find account ", e);
		}
		return account;
	}

}
