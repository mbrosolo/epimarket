/**
 * 
 */
package epitech.java.jcsi.core;

/**
 * @author MaTT
 *
 */
public class DVD extends Product {

	/**
	 * The serialVersionUID
	 */
	private static final long serialVersionUID = 5787668955820800789L;
	/*
	 * The director
	 */
	private String director;

	/**
	 * Constructor
	 *
	 */
	public DVD() {}

	/**
	 * Constructor
	 *
	 * @param name
	 * @param price
	 * @param director
	 */
	public DVD(String name, double price, String director) {this.name = name; this.price = price; this.director = director;}


	/**
	 * @return the director
	 */
	public String getdirector() {return director;}

	/**
	 * @param director the director to set
	 */
	public void setdirector(String director) {this.director = director;}
}
