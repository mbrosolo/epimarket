/**
 * 
 */
package epitech.java.jcsi.core.exception;

/**
 * @author MaTT
 *
 */
public class CategoryException extends ModelException {

	/**
	 * The serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 *
	 * @param message
	 */
	public CategoryException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor
	 *
	 * @param message
	 * @param cause
	 */
	public CategoryException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
