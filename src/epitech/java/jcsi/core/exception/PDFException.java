package epitech.java.jcsi.core.exception;

public class PDFException extends Exception {

	/**
	 * The serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor.
	 * 
	 * @param message
	 *            The error message
	 * @param cause
	 *            The error cause or null
	 */
	public PDFException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PDFException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
