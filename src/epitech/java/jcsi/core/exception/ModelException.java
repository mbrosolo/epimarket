package epitech.java.jcsi.core.exception;

/*
 * ModelException
 * @author Matthias Brosolo
 * @version 1.0, October 2012
 */

public abstract class ModelException extends Exception {

	/**
	 * The serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor.
	 * 
	 * @param message
	 *            The error message
	 * @param cause
	 *            The error cause or null
	 */
	public ModelException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ModelException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
