/**
 * 
 */
package epitech.java.jcsi.core.exception;

/**
 * @author Matthias Brosolo
 *
 */
public class ProductException extends ModelException {

	/**
	 * The SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * Constructor
	 *
	 * @param message
	 */
	public ProductException(String message) {
		super(message);
	}


	/**
	 * Constructor
	 *
	 * @param message
	 * @param cause
	 */
	public ProductException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
