package epitech.java.jcsi.core.exception;

/**
 * Account exception
 * @author sornet_r
 */
public class AccountException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1687366915094774614L;

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            The message
	 * @param cause
	 *            The cause or null
	 */
	public AccountException(String message, Throwable cause) {
		super(message, cause);
	}

	public AccountException(String message) {
		super(message);
	}
}
