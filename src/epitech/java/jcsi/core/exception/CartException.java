package epitech.java.jcsi.core.exception;

public class CartException extends Exception {

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            The message
	 * @param cause
	 *            The cause or null
	 */
	public CartException(String message, Throwable cause) {
		super(message, cause);
	}

	public CartException(String message) {
		super(message);
	}

	/**
	 * The serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

}
