package epitech.java.jcsi.core;

/**
 * Account is an object containing user informations. 
 * @author  Matthias Brosolo
 */
public class Account extends ModelObject {

	/**
	 * The serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The login.
	 */
	private String login = null;

	/**
	 * The digest.
	 */
	private String digest = null;

	/**
	 * The name.
	 */
	private String name = null;
	
	/**
	 * The name.
	 */
	private String surname = null;

	/**
	 * The mail.
	 */
	private String mail = null;
	
	/**
	 * Associated Cart
	 */
	private Cart cart = null;
	
	public Account() {
		super();
	}
	
	/**
	 * Constructor.
	 * 
	 * @param login
	 *            The login
	 * @param digest
	 *            The digest
	 * @param name
	 *            The displayed name
	 * @param surname
	 *            The displayed surname
	 */
	public Account(String login, String digest, String name, String surname, String mail) {
		super();
		this.login = login;
		this.digest = digest;
		this.name = name;
		this.surname = surname;
		this.mail = mail;
		this.cart = new Cart("default");
	}
	
	public String getLogin() {return login;}
	public void setLogin(String login) {this.login = login;}

	public String getDigest() {return digest;}
	public void setDigest(String digest) {this.digest = digest;}

	public String getName() {return name;}
	public void setName(String name) {this.name = name;}

	public String getSurname() {return surname;}
	public void setSurname(String surname) {this.surname = surname;}
	
	public String getMail() {return mail;}
	public void setMail(String mail) {this.mail = mail;}

	public Cart getCart() {return cart;}

	public void setCart(Cart cart) {this.cart = cart;}

	@Override
	public String toString() {
		return "Account [login=" + login + ", digest=" + digest + ", name="
				+ name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((digest == null) ? 0 : digest.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (digest == null) {
			if (other.digest != null)
				return false;
		} else if (!digest.equals(other.digest))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}