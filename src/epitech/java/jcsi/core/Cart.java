package epitech.java.jcsi.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Cart object
 * @author sornet_r
 */
/**
 * @author MaTT
 *
 */
public class Cart extends ModelObject {

	/*
	 * The serialVersionUID
	 */
	private static final long serialVersionUID = -7079602902167130095L;
	
	/*
	 * THe name
	 */
	private String name;
	
	/**
	 * Product list contained in cart
	 */
	private List<OrderLine> orderLines = new ArrayList<OrderLine>();
	
	/**
	 * Constructor
	 *
	 */
	public Cart() {}

	/**
	 * Constructor
	 *
	 * @param name
	 * @param orderLines
	 */
	public Cart(String name) {this.name = name;}

	/**
	 * Add a product to cart
	 * @param productLine
	 * 			ProductLine to add
	 */
	public void addProduct(OrderLine productLine) {orderLines.add(productLine);}

	/**
	 * Get all ProductLine contained in cart
	 * @return
	 * 			List of ProductLine
	 */
	public List<OrderLine> getOrderLines() {return this.orderLines;}
	
	/**
	 * Set all ProductLine contained in cart
	 * @param products
	 * 			ProductLine list
	 */
	public void setOrderLines(List<OrderLine> products) {this.orderLines = products;}
	
	public void addOrderLine(OrderLine order) {this.orderLines.add(order);}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (OrderLine one : orderLines) {
			sb.append(one.toString() + "\n");			
		}
		return sb.toString();
	}

	/**
	 * @return the name
	 */
	public String getName() {return name;}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {this.name = name;}

}
