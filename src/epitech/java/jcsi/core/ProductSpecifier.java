/**
 * 
 */
package epitech.java.jcsi.core;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import epitech.java.jcsi.storage.ObjectSpecifier;
import epitech.java.jcsi.storage.SQLObjectMapper;

/**
 * @author MaTT
 *
 */
public class ProductSpecifier extends ObjectSpecifier {
	
	/**
	 * Constructor
	 *
	 * @param tableName
	 * @param idTableName
	 */
	public ProductSpecifier(SQLObjectMapper mapper, String tableName, String idTableName, String className) {
		super(mapper, tableName, idTableName, className);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Object createSpecifiedAttribute(Connection connection, ResultSet set, String name, String parent)
			throws SQLException, ClassNotFoundException, IntrospectionException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String type = name+"type";
		String tableName = set.getString(type);
		Integer key = set.getInt(name); 
		Class <?> klass = Class.forName(tableName);
		BeanInfo beanInfo;
		beanInfo = Introspector.getBeanInfo(klass);
		System.out.print(name+"\n");
		ResultSet newSet = mapper.createReadStatement(connection, beanInfo, tableName, "id", key);
		System.out.print(name+"\n");
		return mapper.createReadObject(connection, newSet, klass, beanInfo);
	}

}
