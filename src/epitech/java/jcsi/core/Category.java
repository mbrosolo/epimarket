package epitech.java.jcsi.core;

import java.util.List;

/**
 * Category of products. 
 * @author sornet_r
 */
public class Category extends ModelObject {
	
	private static final long serialVersionUID = 738659919190945710L;

	/**
	 * Category name
	 */
	String name = null;
	
	/**
	 * Parent Category
	 */
	Category parentCategory = null;
	
	/**
	 * Child categories
	 */
	List<Category> childCategory = null;
	
	List<Product> products = null;
	
	public Category() {
		
	}
	
	public Category(String name) {
		this.name = name;
	}
	
	public Category(String name, Category parentCategory, List<Category> childCategory, List<Product> products) {
		this.name = name;
		this.parentCategory = parentCategory;
		this.childCategory = childCategory;
		this.products = products;
		this.parentCategory.addChildCategory(this);
		for (Category c : childCategory)
			c.setParentCategory(this);
	}
	
	/**
	 * Add child category to contained categories
	 * @param childCategory
	 */
	public void addChildCategory(Category childCategory) {
		if (this.childCategory.contains(childCategory))
			return;
		this.childCategory.add(childCategory);
		childCategory.setParentCategory(this);
	}
	
	public void delChildCategory(Category childCategory) {
		if (this.childCategory.remove(childCategory))
			childCategory.setParentCategory(null);
	}
	
	public void addProduct(Product product) {
		if (!this.products.contains(product))
			this.products.add(product);
	}
	
	public void delProduct(Product product) {
		this.products.remove(product);
	}

	public String getName() {return this.name;}
	public void setName(String name) {this.name = name;}
	
	public Category getParentCategory() {return this.parentCategory;}
	public void setParentCategory(Category parentCategory) {this.parentCategory = parentCategory; parentCategory.addChildCategory(this);}

	public List<Category> getChildCategory() {return childCategory;}
	public void setChildCategory(List<Category> childCategory) {this.childCategory = childCategory; for (Category c : this.childCategory) c.setParentCategory(this);}

	public List<Product> getProducts() {return products;}
	public void setProducts(List<Product> products) {this.products = products;}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (this.getId() != other.getId())
			return false;
		if (this.getName().equals(other.getName()))
			return false;
		if (this.getProducts().equals(other.getProducts()))
			return false;
		if (this.getChildCategory().equals(other.getChildCategory()))
			return false;
		return true;
	}
}
