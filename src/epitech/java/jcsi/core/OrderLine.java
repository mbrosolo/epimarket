package epitech.java.jcsi.core;

/**
 * Contains product and quantity
 * @author sornet_r
 */
/**
 * @author MaTT
 *
 */
public class OrderLine extends ModelObject {
	
	/*
	 * The serialVersionUID
	 */
	private static final long serialVersionUID = -4103307340248625095L;
	
	/*
	 * The product
	 */
	private Product product = null;
	
	/*
	 * The cart
	 */
	private Cart cart = null;
	
	/*
	 * The product's type
	 */
	private String productType = null;
	
	/*
	 * The quantity
	 */
	private Integer quantity = null;

	/**
	 * Constructor
	 *
	 */
	public OrderLine() { }
	
	/**
	 * Constructor
	 *
	 * @param product
	 * @param quantity
	 */
	public OrderLine(Cart cart, Product product, int quantity) {
		this.cart = cart; this.product = product; this.productType = product.getClass().getName(); this.quantity = quantity;}

	/**
	 * @return
	 */
	public Product getProduct() {return product;}
	
	/**
	 * @param product
	 */
	public void setProduct(Product product) {this.product = product;}

	/**
	 * @return
	 */
	public Integer getQuantity() {return quantity;}
	
	/**
	 * @param quantity
	 */
	public void setQuantity(Integer quantity) {this.quantity = quantity;}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {return "[" + (product != null ? product.getName() : "Undefined") + "]: " + quantity.toString();}

	/**
	 * @return the productType
	 */
	public String getProductType() {return productType;}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {this.productType = productType;}

	/**
	 * @return the cart
	 */
	public Cart getCart() {return cart;}

	/**
	 * @param cart the cart to set
	 */
	public void setCart(Cart cart) {this.cart = cart;}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderLine other = (OrderLine) obj;
		if (this.getId() != other.getId())
			return false;
		if (!this.getCart().equals(other.getCart()))
			return false;
		if (!this.getProduct().equals(other.getProduct()))
			return false;
		if (!this.getQuantity().equals(other.getQuantity()))
			return false;
		return true;
	}
}
