package epitech.java.jcsi.core;

import java.io.Serializable;

/**
 * Marque les objets persistants.
 * 
 * Si un objet de classe <code>Personne</code> contient un attribut
 * <code>copain</code> de type <code>Personne</code>, il faut savoir que
 * <code>copain</code> est persistant. Si cet objet contient un attribut
 * <code>transport</code> dont la valeur courante est de type <code>Bus</code>,
 * il peut être utile de savoir qu'on n'a <i>pas</i> choisi de sauvegarder les
 * <code>Bus</code>.
 */

/**
 * The model which represents all the registered object in database
 * @author Matthias Brosolo
 * @version 1.0, November 2012
 */

public abstract class ModelObject implements Serializable {

	/*
	 * The id which refers to the primary key
	 */
	protected Integer id;
	
	/*
	 * The serialVersionUID
	 */
	private static final long serialVersionUID = 704639020232031475L;

	/**
	 * Return the id
	 * @return Integer
	 */
	public Integer getId() {return this.id;}
	
	/**
	 * Assign the id
	 * @param id
	 */
	public void setId(Integer id) {this.id = id;}
	
	
}
