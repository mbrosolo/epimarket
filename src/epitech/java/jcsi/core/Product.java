package epitech.java.jcsi.core;

/**
 * Product. 
 * @author  Matthias Brosolo
 * @version 1.0, October 2012
 */

public abstract class Product extends ModelObject {

	private static final long serialVersionUID = 704639020232031475L;

	/**
	 * Product name
	 */
	protected String name;

	/**
	 * Product price
	 */
	protected Double price;
	
	/**
	 * List of categories
	 */
		
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}

	public Double getPrice() {return price;}
	public void setPrice(Double price) {this.price = price;}

//	public List<Category> getCategories() {return categories;}
//	public void setCategories(List<Category> categories) {this.categories = categories;}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (this.getId() != other.getId())
			return false;
		return true;
	}
}
