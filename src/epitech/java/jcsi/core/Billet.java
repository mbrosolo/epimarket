/**
 * 
 */
package epitech.java.jcsi.core;

/**
 * @author MaTT
 *
 */
public class Billet extends Product {

	/**
	 */
	private static final long serialVersionUID = -1168396215383054731L;

	/*
	 * The artist
	 */
	private String artist = null;

	/*
	 * The artist
	 */
	private String place = null;

	/**
	 * Constructor
	 *
	 */
	public Billet() {
		// TODO Auto-generated constructor stub
	}

	public Billet(String name, Double price, String artist, String place) {
		this.name = name; this.price = price; this.artist = artist;	this.place = place;}

	/**
	 * @return the artist
	 */
	public String getArtist() {
		return artist;
	}

	/**
	 * @param artist the artist to set
	 */
	public void setArtist(String artist) {
		this.artist = artist;
	}

	/**
	 * @return the place
	 */
	public String getPlace() {
		return place;
	}

	/**
	 * @param place the place to set
	 */
	public void setPlace(String place) {
		this.place = place;
	}

}
