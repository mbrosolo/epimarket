/**
 * 
 */
package epitech.java.jcsi.core;

/**
 * @author MaTT
 *
 */
public class CD extends Product {

	/**
	 * The serialVersionUID
	 */
	private static final long serialVersionUID = 340429161599001665L;

	/*
	 * The artist
	 */
	private String artist;
	
	/**
	 * Constructor
	 *
	 */
	public CD() {}
	
	public CD(String name, double price, String artist) {this.name = name; this.price = price; this.artist = artist;}

	public CD(Product p) {
		if (p.getClass() == this.getClass()) {
			CD tmp = (CD) p;
			this.name = tmp.name; this.price = tmp.price; this.artist = tmp.artist;
		}
	}

	/**
	 * @return the artist
	 */
	public String getArtist() {return artist;}

	/**
	 * @param artist the artist to assign
	 */
	public void setArtist(String artist) {this.artist = artist;}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[" + this.name + ": " + this.price + "/" + this.artist + "]";
	}

}
