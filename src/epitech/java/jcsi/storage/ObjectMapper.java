package epitech.java.jcsi.storage;

import java.util.List;

import epitech.java.jcsi.core.ModelObject;

public interface ObjectMapper {
	
	/**
	 * @param object
	 * 			The object's class
	 * @return Created object
	 * @throws StorageException
	 */
	public ModelObject createObject (ModelObject object) throws StorageException;

	/**
	 * @param klass
	 * 			The object's class
	 * @param primaryKey
	 * 			The object's primary key in storage
	 * @return The read object
	 * @throws StorageException
	 */
	public ModelObject readObject (Class<?> klass, int primaryKey) throws StorageException;

	/**
	 * Update object
	 * 
	 * @param before
	 * 			The object to update
	 * @return The updated object
	 * @throws StorageException
	 */
	public ModelObject updateObject (ModelObject before) throws StorageException;
	
	/**
	 * Delete object from storage
	 * 
	 * @param object
	 * 			The object to delete
	 * @throws StorageException
	 */
	public void deleteObject (ModelObject object) throws StorageException;
	
	/**
	 * find  an object in storage
	 * 
	 * @param klass
	 * 			The Class to find
	 * @param clause
	 * 			The Where clause
	 * @throws StorageException
	 */
	public List<ModelObject> findObject (Class<?> klass, WhereClause clause) throws StorageException;
}