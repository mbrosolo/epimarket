package epitech.java.jcsi.storage;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author MaTT
 *
 */
public abstract class ObjectSpecifier {

	/*
	 * The table's name
	 */
	protected String tableName = null;
	
	/*
	 * the id table's name 
	 */
	protected String idTableName = null;
	
	/*
	 * the id table's name 
	 */
	protected String className = null;
	
	/*
	 * The mapper ref
	 */
	protected SQLObjectMapper mapper = null;

	/**
	 * Constructor
	 *
	 */
	protected ObjectSpecifier(SQLObjectMapper mapper, String tableName, String idTableName, String className) {
		this.mapper = mapper; this.tableName = tableName; this.idTableName = idTableName; this.className = className;}

	protected abstract Object createSpecifiedAttribute(Connection connection, ResultSet set, String name, String parent)
			throws SQLException, ClassNotFoundException, IntrospectionException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException;
	
	/**
	 * @return the tableName
	 */
	public String getTableName() {return tableName;}

	/**
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {this.tableName = tableName;}

	/**
	 * @return the idTableName
	 */
	public String getIdTableName() {return idTableName;}

	/**
	 * @param idTableName the idTableName to set
	 */
	public void setIdTableName(String idTableName) {this.idTableName = idTableName;}

}
