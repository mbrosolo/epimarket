/**
 * 
 */
package epitech.java.jcsi.storage;

import epitech.java.jcsi.core.exception.ModelException;

/**
 * @author MaTT
 *
 */
public class StorageException extends ModelException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 *
	 * @param message
	 */
	public StorageException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor
	 *
	 * @param message
	 * @param cause
	 */
	public StorageException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
