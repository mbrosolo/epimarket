/**
 * 
 */
package epitech.java.jcsi.storage;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import epitech.java.jcsi.core.ModelObject;

/**
 * @author MaTT
 *
 */
public class ListSpecifier extends ObjectSpecifier {

	/**
	 * Constructor
	 *
	 * @param mapper
	 * @param tableName
	 * @param idTableName
	 */
	protected ListSpecifier(SQLObjectMapper mapper, String tableName, String idTableName, String className) {super(mapper, tableName, idTableName, className);}

	/* (non-Javadoc)
	 * @see epitech.java.jcsi.storage.ObjectSpecifier#createSpecifiedAttribute(java.sql.Connection, java.sql.ResultSet, java.lang.String)
	 */
	@Override
	protected Object createSpecifiedAttribute(Connection connection, ResultSet set, String name, String parent) throws SQLException,
			ClassNotFoundException, IntrospectionException,
			InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
//		System.out.print(tableName);
		Integer key = set.getInt("id");
		Class <?> klass = Class.forName(className);
		BeanInfo beanInfo = Introspector.getBeanInfo(klass);
//		System.out.print(className);
		ResultSet newSet = mapper.createReadStatement(connection, beanInfo, tableName, idTableName, key);
		List<ModelObject> list = new ArrayList<ModelObject>();
		while (!newSet.isAfterLast()) {
			ModelObject o = mapper.createReadObject(connection, newSet, klass, beanInfo);
			if (o != null)
				list.add(o);
			else
				break;
		}
		return list;
	}

}
