package epitech.java.jcsi.storage;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Matthias Brosolo
 *
 */
public class WhereClause {

	private StringBuilder clause = new StringBuilder(" WHERE ");
	
	private List<Object> list = new ArrayList<Object>();
	
	private Integer nb;
	
	public WhereClause() {
		nb = 0;
	}
	
	public WhereClause where(String name, Object value) {
		if (nb > 0)
			clause.append("AND ");
		clause.append(name);
		if (value.getClass().isAssignableFrom(String.class))
			clause.append(" LIKE ? ");
		else
			clause.append(" = ? ");
		list.add(value);
		nb = nb + 1;
		return this;
	}
	
	public WhereClause orWhere(String name, Object value) {
		if (nb > 0)
			clause.append("AND ");
		clause.append(name);
		if (value.getClass().isAssignableFrom(String.class))
			clause.append(" LIKE ? ");
		else
			clause.append(" = ? ");
		list.add(value);
		nb = nb + 1;
		return this;
	}

	/**
	 * @return the clause
	 */
	public String getClause() {
		return clause.toString();
	}
	
	public List<Object> getList() {
		return list;
	}
}
