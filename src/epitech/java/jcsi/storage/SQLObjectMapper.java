package epitech.java.jcsi.storage;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import epitech.java.jcsi.core.ModelObject;
import epitech.java.jcsi.storage.ObjectSpecifier;
import epitech.java.jcsi.core.ProductSpecifier;

/**
 * ObjectMapper is an object for database storage. 
 * @author  Matthias Brosolo
 * CRUD Implementation for all objects.
 */

public class SQLObjectMapper implements ObjectMapper {

	/*
	 * The logger
	 */
	private static final Logger LOGGER = Logger.getLogger(SQLObjectMapper.class.getName());
	
	/*
	 * The registered boolean
	 */
	private boolean registered = false;
	
	/*
	 * The config
	 */
	private Map<String, Map<String, ObjectSpecifier>> config = new HashMap<String, Map<String, ObjectSpecifier>>();
	
	/*
	 * The modelObject Factory
	 */
	//private ModelObjectFactory modelObjectFactory;

	/*
	 * The loop list
	 */
	private List<ModelObject> loop = new ArrayList<ModelObject>();
	
	/**
	 * Constructor
	 *
	 */
	public SQLObjectMapper () {
		Map<String, ObjectSpecifier> orderLine = new HashMap<String, ObjectSpecifier>();
		orderLine.put("product", new ProductSpecifier(this, "", "id", ""));
		Map<String, ObjectSpecifier> cart = new HashMap<String, ObjectSpecifier>();
		cart.put("orderLines", new ListSpecifier(this, "orderline", "cart", "epitech.java.jcsi.core.OrderLine"));
		config.put("OrderLine", orderLine);
		config.put("Cart", cart);
	}
	
	/**
	 * Register database driver
	 * 
	 * @throws SQLException
	 */
	private void register() throws SQLException {
		if (!registered) {
			try {
				Class.forName("com.mysql.jdbc.Driver").newInstance();
			} catch (InstantiationException e) {
				throw new SQLException("Failed to register MySQL jdbc driver",
						e);
			} catch (IllegalAccessException e) {
				throw new SQLException("Failed to register MySQL jdbc driver",
						e);
			} catch (ClassNotFoundException e) {
				throw new SQLException("Failed to register MySQL jdbc driver",
						e);
			}
			registered = true;
		}
	}

	
	/**
	 * @return
	 * @throws SQLException
	 */
	protected Connection getConnection() throws SQLException {
		register();
		return DriverManager.getConnection(
				"jdbc:mysql://127.0.0.1:3306/jcsi", "root", "");
	}
		
	public ModelObject createObject (ModelObject object) throws StorageException {
		ModelObject result = null;
		Connection connection = null;
		try {
			connection = getConnection();
			result = doCreateObject(connection, object);
		} catch (SQLException e) {
			throw new StorageException("Database failure", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					LOGGER.log(Level.WARNING,
							"Failed to close MySQL connection properly", e);
				}
			}
		}
		return result;
	}

	protected ModelObject doCreateObject(Connection connection, ModelObject object) throws StorageException {
		// TODO add relations, foreign key
		try {
//			Add if exists check for create object even if linked with existing objects
			List<PropertyDescriptor> collectionList = new ArrayList<PropertyDescriptor>();
			BeanInfo beanInfo = Introspector.getBeanInfo(object.getClass());
			String stringStatement = createStatement(object, beanInfo);
			PreparedStatement statement = connection.prepareStatement(stringStatement, Statement.RETURN_GENERATED_KEYS);
			setStatementValues(connection, object, statement, beanInfo, collectionList);
			LOGGER.warning(statement.toString());
			if (statement.executeUpdate() == 0)
				throw new StorageException("Can't create new entry");
			ResultSet keys = statement.getGeneratedKeys();
			if (keys.next()) {
				object.setId(keys.getInt(1));
				createCollections(connection, object, collectionList);
			}
		} catch	(IntrospectionException e) {
			throw new StorageException("Introspection failure", e);
		} catch (SQLException e) {
			throw new StorageException("Database failure", e);
		} catch (IllegalArgumentException e) {
			throw new StorageException("Illegal Argument", e);
		}
		return object;
	}

	protected void createCollections(Connection connection, ModelObject object, List<PropertyDescriptor> collectionList) throws StorageException {
		for (PropertyDescriptor element : collectionList) {
			try {
				String name = object.getClass().getSimpleName();
				@SuppressWarnings("unchecked")
				List<ModelObject> list = (List<ModelObject>) element.getReadMethod().invoke(object);
				for (ModelObject listObject : list) {
					BeanInfo beanInfo = Introspector.getBeanInfo(listObject.getClass());
					for (PropertyDescriptor attribute : beanInfo.getPropertyDescriptors()) {
						if (name.equals(attribute.getName()))
							attribute.getWriteMethod().invoke(listObject, object);
					}
					doCreateObject(connection, listObject);
				}
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | IntrospectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	}
	
	private String createStatement(ModelObject object, BeanInfo beanInfo) {
		StringBuilder buffer = new StringBuilder("INSERT INTO ");
		StringBuilder bufferValues = new StringBuilder(" VALUES (");
		String tableName = object.getClass().getName().replaceAll("[^\\.]+\\.", "");
		buffer.append(tableName).append(" (");
		boolean first = true;
		String sep = ", ";
		for (PropertyDescriptor attribute : beanInfo.getPropertyDescriptors()) {
			String name = attribute.getName();
			if (!"class".equals(name) && !"id".equals(name) && !Collection.class.isAssignableFrom(attribute.getPropertyType())) {
				if (!first) {
					buffer.append(sep);
					bufferValues.append(sep);
				}
				first = false;
				buffer.append(name);
				bufferValues.append("?");
			}// else
//				first = true;
		}
		buffer.append(")");
		bufferValues.append(")");
		buffer.append(bufferValues);
		//			LOGGER.log(Level.WARNING, buffer.toString());
		return buffer.toString();
	}
	
	private void setStatementValues (Connection connection, ModelObject object, PreparedStatement statement,
									 BeanInfo beanInfo, List<PropertyDescriptor> collectionList) throws StorageException {
		Integer i = new Integer(1);
		try {
			for (PropertyDescriptor attribute : beanInfo.getPropertyDescriptors()) {
				String name = attribute.getName();
				if (!"class".equals(name) && !"id".equals(name)) {
					Class<?> klass = attribute.getPropertyType();
					//					LOGGER.warning(klass.getName());
					Object parameter = attribute.getReadMethod().invoke(object);
					//					LOGGER.warning(parameter.toString());
					if (klass.isAssignableFrom(String.class))
						statement.setString(i, (String) parameter);
					else if (klass.isAssignableFrom(Integer.class))
						statement.setInt(i, (int) parameter);
					else if (klass.isAssignableFrom(Double.class))
						statement.setDouble(i, (double) parameter);
					else if (klass.isAssignableFrom(Long.class))
						statement.setLong(i, (long) parameter);
					else if (ModelObject.class.isAssignableFrom(klass)) {
						ModelObject ref = (ModelObject) attribute.getReadMethod().invoke(object);
						Integer id = (ref.getId() != null ? ref : (ModelObject) doCreateObject(connection, ref)).getId();
						statement.setInt(i, id);
					} else if (Collection.class.isAssignableFrom(klass)) {
						collectionList.add(attribute);
//						Add 1-n and n-n relations
						LOGGER.log(Level.WARNING, "Collections");
					} else {
						LOGGER.log(Level.WARNING, name);
						throw new StorageException ("Undefined type attribute: "+ name + ".");
					}
					++i;
				}
				//				LOGGER.warning(i.toString());
			}
		} catch (IllegalAccessException e) {
			throw new StorageException("Illegal Access", e);
		} catch (InvocationTargetException e) {
			throw new StorageException("Invocation Error", e);
		} catch (SQLException e) {
			throw new StorageException("Database failure", e);
		}

	}

	public ModelObject readObject (Class<?> klass, int primaryKey) throws StorageException {
		ModelObject result = null;
		Connection connection = null;
		try {
			connection = getConnection();
			result = doReadObject(connection, klass, primaryKey);
		} catch (SQLException e) {
			throw new StorageException("Database failure", e);
		} catch (InstantiationException e) {
			throw new StorageException("Instantiation failure", e);
		} catch (IllegalAccessException e) {
			throw new StorageException("Illegal Access failure", e);
		} catch (IllegalArgumentException e) {
			throw new StorageException("Illegal Argument failure", e);
		} catch (InvocationTargetException e) {
			throw new StorageException("Invocation Target failure", e);
		} catch (IntrospectionException e) {
			throw new StorageException("Introspection failure", e);
		} catch (ClassNotFoundException e) {
			throw new StorageException("Class not Found", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					LOGGER.log(Level.WARNING,
							"Failed to close MySQL connection properly", e);
				}
			}
		}
		return result;
	}
	
	/**
	 * @param c
	 * @param klass
	 * @param key
	 * @return
	 * @throws IntrospectionException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws ClassNotFoundException 
	 */
	protected ModelObject doReadObject(Connection connection, Class<?> klass, int key)
			throws IntrospectionException, InstantiationException,
			IllegalAccessException, SQLException, IllegalArgumentException,
			InvocationTargetException, ClassNotFoundException {
		LOGGER.warning("DAT KLASS = " + klass.getSimpleName());
		// TODO add relations, foreign key
		BeanInfo beanInfo = Introspector.getBeanInfo(klass);
		ResultSet set =  createReadStatement(connection, beanInfo, klass.getName(), "id", key);
		ModelObject object = createReadObject(connection, set, klass, beanInfo);
		return object;
	}

	public ResultSet createReadStatement(Connection connection, BeanInfo beanInfo, String tableName, String idTableName, int key)
			throws SQLException {
		StringBuilder buffer = new StringBuilder("SELECT ");
		//			for (PropertyDescriptor attribute : beanInfo.getPropertyDescriptors()) {
		//
		//			}
		buffer.append("*");
		buffer.append(" FROM ");
		String tableNameReplaced = tableName.replaceAll("[^\\.]+\\.", "");
		buffer.append(tableNameReplaced).append(" WHERE ").append(idTableName).append(" = ?");
		PreparedStatement statement = connection.prepareStatement(buffer.toString());
		statement.setInt(1, key);
				LOGGER.warning(statement.toString());
		return statement.executeQuery();
	}

	private ModelObject isInList(List<ModelObject> list, int id, Class <?> klass) {
		for (ModelObject o : list) {
			if (o.getClass() == klass && o.getId() == id)
				return o;
		}
		return null;
	}
	
	public ModelObject createReadObject(Connection connection, ResultSet set, Class<?> klass, BeanInfo beanInfo)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SQLException, IntrospectionException {
		ModelObject object = null;
		Map<String, ObjectSpecifier> map = null;
		if (set.next()) {
			if (config.containsKey(klass.getSimpleName()))
				map = (Map<String, ObjectSpecifier>) config.get(klass.getSimpleName());
			System.out.print("New instance of "+ klass.getSimpleName() + "\n");
			object = (ModelObject) klass.newInstance();
			ModelObject tmp = isInList(loop, set.getInt("id"), klass);
			if (tmp == null)
				loop.add(object);
			else
				return tmp;
			for (PropertyDescriptor attribute : beanInfo.getPropertyDescriptors()) {
				String name = attribute.getName();
				if (!"class".equals(name)) {
					Class<?> subKlass = attribute.getPropertyType();
//											LOGGER.warning(name);
					//						LOGGER.warning(subKlass.getName());
					LOGGER.warning(name);
					if (ModelObject.class.isAssignableFrom(subKlass) || Collection.class.isAssignableFrom(subKlass)) {
						Object specialObject = null;
						if (map != null && map.containsKey(name)) {
							LOGGER.warning("Specific");
							ObjectSpecifier spec = (ObjectSpecifier) map.get(name);
							specialObject = spec.createSpecifiedAttribute(connection, set, name, klass.getSimpleName());
						} else {
							LOGGER.warning("or not");
							specialObject = doReadObject(connection, subKlass, set.getInt(name));
						}
						System.out.print(name + " Gotcha\n");
						attribute.getWriteMethod().invoke(object, specialObject);
//					} else if (Collection.class.isAssignableFrom(subKlass)) {
						//						if (attribute.getPropertyType() == null)
//							LOGGER.warning("NULL");
//						LOGGER.warning(attribute.getPropertyType().getComponentType());
					} else
						attribute.getWriteMethod().invoke(object, set.getObject(name));
				}
			}
		} else
			LOGGER.warning("No entry to read.");
		return object;
	}

	public ModelObject updateObject (ModelObject object) throws StorageException {
		// TODO UPDATE in database 
		// TODO Add ON UPDATE manage, relations UPDATE
		Connection connection = null;
		try {
			connection = getConnection();
			doUpdateObject(connection, object);
		} catch (SQLException e) {
			throw new StorageException("Database failure", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					LOGGER.log(Level.WARNING,
							"Failed to close MySQL connection properly", e);
				}
			}
		}
		return object;
	}

	public Object doUpdateObject (Connection connection, ModelObject object) throws StorageException {
		try {
			List<PropertyDescriptor> collectionList = new ArrayList<PropertyDescriptor>();
			BeanInfo beanInfo = Introspector.getBeanInfo(object.getClass());
			String stringStatement = createUpdateStatement(object, beanInfo);
			PreparedStatement statement = connection.prepareStatement(stringStatement, Statement.RETURN_GENERATED_KEYS);
			setStatementUpdate(connection, object, statement, beanInfo, collectionList);
			LOGGER.warning(statement.toString());
			if (statement.executeUpdate() == 0)
				throw new StorageException("Can't update entry");
//			ResultSet keys = statement.getGeneratedKeys();
//			if (keys.next()) {
//				object.setId(keys.getInt(1));
//				createCollections(connection, object, collectionList);
//			}
		} catch	(IntrospectionException e) {
			throw new StorageException("Introspection failure", e);
		} catch (SQLException e) {
			throw new StorageException("Database failure", e);
		} catch (IllegalArgumentException e) {
			throw new StorageException("Illegal Argument", e);
		}
		return object;
	}
	
	private String createUpdateStatement(ModelObject object, BeanInfo beanInfo) {
		StringBuilder buffer = new StringBuilder("UPDATE ");
		String tableName = object.getClass().getName().replaceAll("[^\\.]+\\.", "");
		buffer.append(tableName).append(" SET ");
		boolean first = true;
		String sep = ", ";
		for (PropertyDescriptor attribute : beanInfo.getPropertyDescriptors()) {
			String name = attribute.getName();
			if (!"class".equals(name) && !"id".equals(name) && !Collection.class.isAssignableFrom(attribute.getPropertyType())) {
				if (!first) {
					buffer.append(sep);
				}
				first = false;
				buffer.append(name).append("= ?");
			}
		}
		buffer.append(" WHERE id = ?");
		return buffer.toString();
	}
	
	private void setStatementUpdate (Connection connection, ModelObject object, PreparedStatement statement,
			BeanInfo beanInfo, List<PropertyDescriptor> collectionList) throws StorageException {
		Integer i = new Integer(1);
		try {
			for (PropertyDescriptor attribute : beanInfo.getPropertyDescriptors()) {
				String name = attribute.getName();
				if (!"class".equals(name) && !"id".equals(name)) {
					Class<?> klass = attribute.getPropertyType();
					//					LOGGER.warning(klass.getName());
					Object parameter = attribute.getReadMethod().invoke(object);
					//					LOGGER.warning(parameter.toString());
					if (klass.isAssignableFrom(String.class))
						statement.setString(i, (String) parameter);
					else if (klass.isAssignableFrom(Integer.class))
						statement.setInt(i, (int) parameter);
					else if (klass.isAssignableFrom(Double.class))
						statement.setDouble(i, (double) parameter);
					else if (klass.isAssignableFrom(Long.class))
						statement.setLong(i, (long) parameter);
					else if (ModelObject.class.isAssignableFrom(klass)) {
						ModelObject ref = (ModelObject) attribute.getReadMethod().invoke(object);
						Integer id = (ref.getId() != null ? ref : (ModelObject) doCreateObject(connection, ref)).getId();
						statement.setInt(i, id);
					} else if (Collection.class.isAssignableFrom(klass)) {
						collectionList.add(attribute);
						//Add 1-n and n-n relations
						LOGGER.log(Level.WARNING, "Collections");
					} else {
						LOGGER.log(Level.WARNING, name);
						throw new StorageException ("Undefined type attribute: "+ name + ".");
					}
					++i;
				}
				//				LOGGER.warning(i.toString());
			}
			statement.setInt(i, object.getId());
		} catch (IllegalAccessException e) {
			throw new StorageException("Illegal Access", e);
		} catch (InvocationTargetException e) {
			throw new StorageException("Invocation Error", e);
		} catch (SQLException e) {
			throw new StorageException("Database failure", e);
		}

	}
	
	public void deleteObject (ModelObject object) throws StorageException {
		//TODO Add manage of ON DELETE
		try {
			StringBuilder buffer = new StringBuilder("DELETE FROM ");
			Class<?> klass = object.getClass();
			String tableName = klass.getName().replaceAll("[^\\.]+\\.", "");
			buffer.append(tableName).append(" WHERE id = ?");
			PreparedStatement statement = getConnection().prepareStatement(buffer.toString());
			Integer key = object.getId();
			if (key != null)
				statement.setInt(1, key);
			else
				throw new StorageException("Id is null");
			if (statement.executeUpdate() == 0)
				LOGGER.warning("No entry to delete for this id.");
		} catch (SQLException e) {
			throw new StorageException("Database failure", e);
		} catch (IllegalArgumentException e) {
			throw new StorageException("Illegal Argument", e);
		}
	}
	
	public List<ModelObject> findObject(Class<?> klass, WhereClause clause) throws StorageException {
		List<ModelObject> result = new ArrayList<ModelObject>();
		Connection connection = null;
		try {
			connection = getConnection();
			doFindObject(connection, klass, clause, result);
		} catch (SQLException e) {
			throw new StorageException("Database failure", e);
		} catch (InstantiationException e) {
			throw new StorageException("Instantiation failure", e);
		} catch (IllegalAccessException e) {
			throw new StorageException("Illegal Access failure", e);
		} catch (IllegalArgumentException e) {
			throw new StorageException("Illegal Argument failure", e);
		} catch (InvocationTargetException e) {
			throw new StorageException("Invocation Target failure", e);
		} catch (IntrospectionException e) {
			throw new StorageException("Introspection failure", e);
		} catch (ClassNotFoundException e) {
			throw new StorageException("Class not Found", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					LOGGER.log(Level.WARNING,
							"Failed to close MySQL connection properly", e);
				}
			}
		}
		return result;
	}
	
	protected void doFindObject(Connection connection, Class<?> klass, WhereClause clause, List<ModelObject> result)
			throws IntrospectionException, InstantiationException,
			IllegalAccessException, SQLException, IllegalArgumentException,
			InvocationTargetException, ClassNotFoundException {
		BeanInfo beanInfo = Introspector.getBeanInfo(klass);
		ResultSet set =  createFindStatement(connection, beanInfo, klass.getName(), clause);
		 createFindObject(connection, set, klass, beanInfo, result);
	}

	public ResultSet createFindStatement(Connection connection, BeanInfo beanInfo, String tableName, WhereClause clause)
			throws SQLException {
		StringBuilder buffer = new StringBuilder("SELECT ");
		//			for (PropertyDescriptor attribute : beanInfo.getPropertyDescriptors()) {
		//
		//			}
		buffer.append("*");
		buffer.append(" FROM ");
		String tableNameReplaced = tableName.replaceAll("[^\\.]+\\.", "");
		buffer.append(tableNameReplaced).append(clause.getClause());
		LOGGER.warning(buffer.toString());
		PreparedStatement statement = connection.prepareStatement(buffer.toString());
		Integer i = 0;
		for (Object parameter : clause.getList()) {
			i = i + 1;
			Class<?> subKlass = parameter.getClass();
			if (subKlass.isAssignableFrom(String.class))
				statement.setString(i, (String) parameter);
			else if (subKlass.isAssignableFrom(Integer.class))
				statement.setInt(i, (int) parameter);
			else if (subKlass.isAssignableFrom(Double.class))
				statement.setDouble(i, (double) parameter);

		}
		return statement.executeQuery();
	}
	
	public void createFindObject(Connection connection, ResultSet set, Class<?> klass, BeanInfo beanInfo, List<ModelObject> result)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, SQLException, IntrospectionException {
		ModelObject object = null;
		Map<String, ObjectSpecifier> map = null;
		while (set.next()) {
			if (config.containsKey(klass.getSimpleName()))
				map = (Map<String, ObjectSpecifier>) config.get(klass.getSimpleName());
			System.out.print("New instance of "+ klass.getSimpleName() + "\n");
			object = (ModelObject) klass.newInstance();
			ModelObject tmp = isInList(loop, set.getInt("id"), klass);
			if (tmp == null)
				loop.add(object);
//			else
//				return tmp;
			for (PropertyDescriptor attribute : beanInfo.getPropertyDescriptors()) {
				String name = attribute.getName();
				if (!"class".equals(name)) {
					Class<?> subKlass = attribute.getPropertyType();
//											LOGGER.warning(name);
					//						LOGGER.warning(subKlass.getName());
					LOGGER.warning(name);
					if (ModelObject.class.isAssignableFrom(subKlass) || Collection.class.isAssignableFrom(subKlass)) {
						Object specialObject = null;
						if (map != null && map.containsKey(name)) {
							LOGGER.warning("Specific");
							ObjectSpecifier spec = (ObjectSpecifier) map.get(name);
							specialObject = spec.createSpecifiedAttribute(connection, set, name, klass.getSimpleName());
						} else {
							LOGGER.warning("or not");
							specialObject = doReadObject(connection, subKlass, set.getInt(name));
						}
						System.out.print(name + " Gotcha\n");
						attribute.getWriteMethod().invoke(object, specialObject);
//					} else if (Collection.class.isAssignableFrom(subKlass)) {
						//						if (attribute.getPropertyType() == null)
//							LOGGER.warning("NULL");
//						LOGGER.warning(attribute.getPropertyType().getComponentType());
					} else
						attribute.getWriteMethod().invoke(object, set.getObject(name));
				}
			}
			result.add(object);
		}
	}
	
}
