package epitech.java.epimarket.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import epitech.java.epimarket.bean.EpiMarketBean;
import epitech.java.jcsi.core.Product;
import epitech.java.jcsi.storage.StorageException;

/**
 * @author Matthias Brosolo
 *
 */
public class ProductServlet extends HttpServlet {
	
	/**
	 * The SerialVersionUID
	 */
	private static final long serialVersionUID = -3480844129939515065L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EpiMarketBean epi = null;
		try {
			epi = EpiMarketBean.getInstance();
		} catch (StorageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		List<Product> list = epi.getAllProducts(); 
		System.out.print(list.size());
		request.setAttribute("productList", list);
		this.getServletContext().getRequestDispatcher( "/product/viewProduct.jsp" ).forward(request, response);
	}
}
