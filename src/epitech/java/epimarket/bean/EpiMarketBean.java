package epitech.java.epimarket.bean;

import java.util.List;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import epitech.java.jcsi.core.Account;
import epitech.java.jcsi.core.Product;
import epitech.java.jcsi.business.AccountManager;
import epitech.java.jcsi.business.ProductManager;
import epitech.java.jcsi.core.exception.AccountException;
import epitech.java.jcsi.core.exception.ProductException;
import epitech.java.jcsi.storage.ObjectMapper;
import epitech.java.jcsi.storage.SQLObjectMapper;
import epitech.java.jcsi.storage.StorageException;

/**
 * @author Matthias Brosolo
 *
 */
public class EpiMarketBean {

	private static final Logger LOGGER = Logger.getLogger(EpiMarketBean.class
			.getName());
	
	private AccountManager accountManager;
	private ProductManager productManager;
	private ObjectMapper objectMapper;
	
	private static EpiMarketBean instance = null;
	private static Object sync__;
	
	public static EpiMarketBean getInstance() throws StorageException {
		
		 if (null == instance) {
//	            synchronized(sync__) {
	                if (null == instance) {
	                    instance = new EpiMarketBean();
	                }
//	            }
	        }
		return instance;
	}
	
	public EpiMarketBean() throws StorageException {
		initUsingMysqlStorage();
	}
	
	private void initUsingMysqlStorage() throws StorageException {
		this.objectMapper = new SQLObjectMapper();
		this.accountManager = new AccountManager(objectMapper);
		this.productManager = new ProductManager(objectMapper);
		LOGGER.info("Storage initialized to MySQL");
	}
	
	/**
	 * Register a new account in database
	 * @return String
	 */
	public String register() {
		RegisterBean registerBean = (RegisterBean) FacesContext.getCurrentInstance()
				.getExternalContext().getRequestMap().get("registerBean");
		Account account = null;
		try {
			account = this.accountManager.registerAccount(registerBean.getLogin(), registerBean.getPassword(),
					registerBean.getName(), registerBean.getSurname(), registerBean.getMail());
		} catch (AccountException e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage("login",
					new FacesMessage("Ce compte existe d�j� ", e.getMessage()));
			return "failure";
		}
		LOGGER.warning("Success");
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.put("account", account);		
		return "success";
	}

	/**
	 * Login user
	 * @return String
	 */
	public String login() {
		LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance()
				.getExternalContext().getRequestMap().get("loginBean");
		Account account = null;
		try {
			account = this.accountManager.loginAccount(loginBean.getLogin(), loginBean.getPassword());
		} catch (AccountException e) {
			LOGGER.warning("Failure");
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage("login",
					new FacesMessage("Ce compte n'existe pas ", e.getMessage()));
			return "failure";
		}
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
				.put("account", account);		
		return "success";
	}
	
	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("account");
		return "success";
	}
	
	public List<Product> getAllProducts() {
		List<Product> list = null;
		try {
			list = productManager.getAllProducts();
		} catch (ProductException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.print("getAll failed\n");
		}
		return list;
	}
}
