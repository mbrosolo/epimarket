/**
 * 
 */
package epitech.java.epimarket.bean;

import java.io.Serializable;

/**
 * @author MaTT
 *
 */
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 8702666212205181920L;
	private String login;
	private String password;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
