/**
 * 
 */
package epitech.java.epimarket.bean;

import java.io.Serializable;

import org.apache.catalina.util.MD5Encoder;

import sun.security.provider.MD5;
import sun.security.rsa.RSASignature.MD5withRSA;

/**
 * @author MaTT
 *
 */
public class RegisterBean implements Serializable {

	/**
	 * The serialVersionUID
	 */
	private static final long serialVersionUID = -6801410732095152498L;

	/**
	 * The login.
	 */
	private String login = null;

	/**
	 * The password.
	 */
	private String password = null;

	/**
	 * The name.
	 */
	private String name = null;
	
	/**
	 * The name.
	 */
	private String surname = null;

	/**
	 * The mail.
	 */
	private String mail = null;
	
	public RegisterBean() {
		
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
}
