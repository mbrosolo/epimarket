package epitech.java.jcsi.buisness;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import epitech.java.jcsi.business.CartManager;
import epitech.java.jcsi.core.CD;
import epitech.java.jcsi.core.Cart;
import epitech.java.jcsi.core.DVD;
import epitech.java.jcsi.core.exception.CartException;
import epitech.java.jcsi.storage.ObjectMapper;
import epitech.java.jcsi.storage.SQLObjectMapper;

public class CartManagerTest {
	ObjectMapper mapper = new SQLObjectMapper();
	CartManager manager = new CartManager(mapper);
	
	@Test
	public void cartManagerTest() {
		Cart c1 = new Cart();
		
		CD cd = new CD("TestCD-Cart", 20, "TestCD-Artist");
		DVD dvd = new DVD("TestDVD-Cart", 20, "TestDVD-Artist");
		
		try {
			this.manager.addToCart(c1, cd, 2);
			this.manager.addToCart(c1, dvd, 1);
		} catch (CartException e) {
			fail("Can't add product to Cart");
		}
		
		try {
			this.manager.updateCart(c1, dvd, 0);
		} catch (CartException e) {
			fail("Can't delete product from cart");
		}
		
		Cart test = null;
		try {
			test = this.manager.getCart(c1.getId());
		} catch (CartException e) {
			fail("Can't get from cart ID");
		}
		
		assertEquals(c1, test);
	}
}
