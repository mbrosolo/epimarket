package epitech.java.jcsi.buisness;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import epitech.java.jcsi.business.CategoryManager;
import epitech.java.jcsi.core.CD;
import epitech.java.jcsi.core.Category;
import epitech.java.jcsi.core.DVD;
import epitech.java.jcsi.core.exception.CategoryException;
import epitech.java.jcsi.storage.ObjectMapper;
import epitech.java.jcsi.storage.SQLObjectMapper;

public class CategoryManagerTest {
	ObjectMapper mapper = new SQLObjectMapper();
	CategoryManager manager = new CategoryManager(mapper);
	
	@Test
	public void categoryManagerTest() {
		Category root = new Category("Root");
		
		try {
			this.manager.creatCategory(root);
		} catch (CategoryException e) {
			fail("Couldn't creat category");
		}
		
		Category c1 = new Category("c1");		
		Category c2 = new Category("c2");
		c1.setParentCategory(root);
		c2.setParentCategory(root);
		try {
			this.manager.addChildCategory(root, c1);
			this.manager.addChildCategory(root, c2);
		} catch (CategoryException e) {
			fail("Couldn't add child category");
		}
		
		CD cd = new CD("TestCD-Category", 20, "TestCD-Artist");
		DVD dvd = new DVD("TestDVD-Category", 20, "TestDVD-Artist");
		try {
			this.manager.addProduct(c2, dvd);
			this.manager.addProduct(c2, cd);
		} catch (CategoryException e) {
			fail("Can't add product to category");
		}
		
		Category test = null;
		try {
			this.manager.getCategory(root.getId());
		} catch (CategoryException e) {
			fail("Couldn't read category from id");
		}
		
		assertEquals(root, test);
		
		try {
			this.manager.delCategory(c2);
		} catch ( CategoryException e) {
			fail("Can't delete category");
		}
		
		int error = 0;
		try {
			test = this.manager.getCategory(c2.getId());
		} catch (CategoryException e) {
			++error;
		}
		if (error == 0)
			fail("Didnt deleted the category into DB");
		
		test = null;
		try {
			test = this.manager.getCategory(root.getId());
		} catch (CategoryException e) {
			fail("Can't load from DB");
		}
		
		if (test.getChildCategory().size() != 1)
			fail("Wrong delete was done into DB");
	}
}
