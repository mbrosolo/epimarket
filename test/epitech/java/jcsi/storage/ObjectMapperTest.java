package epitech.java.jcsi.storage;

import static org.junit.Assert.*;

import org.junit.Test;
import epitech.java.jcsi.core.Account;

/**
 * JUnit Test for ObjectMapper
 * @author Matthias Brosolo
 * @version 1.0, October 2012
 */
public class ObjectMapperTest {

	@Test
	public void testObjectMapper() {
		SQLObjectMapper mapper = new SQLObjectMapper();
		Account acc = new Account("brosol_m", "passwd", "matthias", "brosolo", "matthias.brosolo@epitech.eu");
//		Account acc2 = new Account("brosol_n", "passwd", "matthias", "brosolo", "natthias.brosolo@epitech.eu");
		try {
			Account tmp = (Account) mapper.createObject(acc);
//			System.out.print(tmp.getId());
//			Account tmp2 = (Account) mapper.createObject(acc2);
			Account copy = (Account) mapper.readObject(Account.class, tmp.getId());
			assertEquals(tmp, copy);
			try {
				mapper.deleteObject(copy);
			} catch (StorageException e) {
				System.out.print("Can't delete entry");				
			}
			try {
				mapper.readObject(Account.class, tmp.getId());
			} catch (StorageException e) {
				System.out.print("Can't read entry");
			}
		} catch (StorageException e) {
			e.printStackTrace();
			fail();
		}
	}

}
