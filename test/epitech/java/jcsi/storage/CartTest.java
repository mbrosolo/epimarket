package epitech.java.jcsi.storage;

import org.junit.Test;

import epitech.java.jcsi.business.CartManager;
import epitech.java.jcsi.core.Billet;
import epitech.java.jcsi.core.CD;
import epitech.java.jcsi.core.Cart;
import epitech.java.jcsi.core.Product;
import epitech.java.jcsi.core.exception.CartException;

public class CartTest {

	@Test
	public void cartTest() {
		try {
			SQLObjectMapper mapper = new SQLObjectMapper();
			Cart cart = new Cart("My cart 2");
//			Product cd1 = (Product) mapper.readObject(Class.forName("epitech.java.jcsi.core.CD"), 8);
//			Product cd2 = (Product) mapper.readObject(Class.forName("epitech.java.jcsi.core.CD"), 9);
			Product cd3 = new CD("Homework", 10.99, "Daft Punk");
			Product cd4 = new CD("Discovery", 11.99, "Daft Punk");
			Product billet1 = new Billet("Holiday on Ice", 15.00, "Disney", "Paris");
			CartManager cm = new CartManager(mapper);
			cm.addToCart(cart, cd3, 1);
			cm.addToCart(cart, cd4, 2);
			cm.addToCart(cart, billet1, 3);
			System.out.print(cart.toString());
			Cart cart2 = null;
			mapper.createObject(cart);
//			System.out.print(cart.toString());			
//			assertEquals(cart, cart2);
			cart2 = (Cart) mapper.readObject(Class.forName("epitech.java.jcsi.core.Cart"), 23);
			System.out.print(cart2.toString());			
			//			assertEquals(cart, cart2);
			mapper.deleteObject(cart);
			mapper.deleteObject(cd3);
			mapper.deleteObject(cd4);
			mapper.deleteObject(billet1);
		} catch (StorageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.print("Catch");								
		} catch (CartException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
