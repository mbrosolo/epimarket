package epitech.java.jcsi.storage;

import static org.junit.Assert.*;

import org.junit.Test;

import epitech.java.jcsi.core.CD;
import epitech.java.jcsi.core.Product;

public class UpdateTest {

	@Test
	public void test() {
		try {
			SQLObjectMapper mapper = new SQLObjectMapper();
			Product cd1 = (Product) mapper.readObject(Class.forName("epitech.java.jcsi.core.CD"), 8);
//			Product cd2 = (Product) mapper.readObject(Class.forName("epitech.java.jcsi.core.CD"), 8);
			Product cd2 = new CD(cd1);
			cd1.setPrice(20.99);
			mapper.updateObject(cd1);
			System.out.print(cd1.toString()+ "\n");			
			System.out.print(cd2.toString());			
			assertFalse(cd1 == cd2);
//			mapper.deleteObject(cart);
//			mapper.deleteObject(cd3);
//			mapper.deleteObject(cd4);
//			mapper.deleteObject(billet1);
		} catch (StorageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.print("Catch");								
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
