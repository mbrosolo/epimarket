package epitech.java.jcsi.storage;

import static org.junit.Assert.*;

import org.junit.Test;

import epitech.java.jcsi.core.CD;
import epitech.java.jcsi.core.OrderLine;
import epitech.java.jcsi.core.Product;

public class ReferenceTest {

	@Test
	public void test() {
		SQLObjectMapper mapper = new SQLObjectMapper();
		Product cd1 = new CD("CIRCLE", 9.99, "Shibasaki Kou");
//		Set a cart for the OrderLine
		OrderLine order = new OrderLine(null, cd1, 2);
		try {
			mapper.createObject(order);
		} catch (StorageException e) {
			// TODO Auto-generated catch block
			System.out.print("Mapper failed.");
			e.printStackTrace();
			fail();
		}
	}

}
